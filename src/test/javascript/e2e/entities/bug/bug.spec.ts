import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { BugComponentsPage, BugDeleteDialog, BugUpdatePage } from './bug.page-object';

const expect = chai.expect;

describe('Bug e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let bugComponentsPage: BugComponentsPage;
  let bugUpdatePage: BugUpdatePage;
  let bugDeleteDialog: BugDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Bugs', async () => {
    await navBarPage.goToEntity('bug');
    bugComponentsPage = new BugComponentsPage();
    await browser.wait(ec.visibilityOf(bugComponentsPage.title), 5000);
    expect(await bugComponentsPage.getTitle()).to.eq('crowdtestingApp.bug.home.title');
  });

  it('should load create Bug page', async () => {
    await bugComponentsPage.clickOnCreateButton();
    bugUpdatePage = new BugUpdatePage();
    expect(await bugUpdatePage.getPageTitle()).to.eq('crowdtestingApp.bug.home.createOrEditLabel');
    await bugUpdatePage.cancel();
  });

  it('should create and save Bugs', async () => {
    const nbButtonsBeforeCreate = await bugComponentsPage.countDeleteButtons();

    await bugComponentsPage.clickOnCreateButton();
    await promise.all([bugUpdatePage.deviceSelectLastOption(), bugUpdatePage.testerSelectLastOption()]);
    await bugUpdatePage.save();
    expect(await bugUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await bugComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Bug', async () => {
    const nbButtonsBeforeDelete = await bugComponentsPage.countDeleteButtons();
    await bugComponentsPage.clickOnLastDeleteButton();

    bugDeleteDialog = new BugDeleteDialog();
    expect(await bugDeleteDialog.getDialogTitle()).to.eq('crowdtestingApp.bug.delete.question');
    await bugDeleteDialog.clickOnConfirmButton();

    expect(await bugComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
