import { element, by, ElementFinder } from 'protractor';

export class BugComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-bug div table .btn-danger'));
  title = element.all(by.css('jhi-bug div h2#page-heading span')).first();

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class BugUpdatePage {
  pageTitle = element(by.id('jhi-bug-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  deviceSelect = element(by.id('field_device'));
  testerSelect = element(by.id('field_tester'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async deviceSelectLastOption(): Promise<void> {
    await this.deviceSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async deviceSelectOption(option: string): Promise<void> {
    await this.deviceSelect.sendKeys(option);
  }

  getDeviceSelect(): ElementFinder {
    return this.deviceSelect;
  }

  async getDeviceSelectedOption(): Promise<string> {
    return await this.deviceSelect.element(by.css('option:checked')).getText();
  }

  async testerSelectLastOption(): Promise<void> {
    await this.testerSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async testerSelectOption(option: string): Promise<void> {
    await this.testerSelect.sendKeys(option);
  }

  getTesterSelect(): ElementFinder {
    return this.testerSelect;
  }

  async getTesterSelectedOption(): Promise<string> {
    return await this.testerSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class BugDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-bug-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-bug'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
