package me.phrycenko.crowdtesting.web.rest;

import me.phrycenko.crowdtesting.CrowdtestingApp;
import me.phrycenko.crowdtesting.domain.Bug;
import me.phrycenko.crowdtesting.repository.BugRepository;
import me.phrycenko.crowdtesting.service.BugService;
import me.phrycenko.crowdtesting.service.dto.BugDTO;
import me.phrycenko.crowdtesting.service.mapper.BugMapper;
import me.phrycenko.crowdtesting.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static me.phrycenko.crowdtesting.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link BugResource} REST controller.
 */
@SpringBootTest(classes = CrowdtestingApp.class)
public class BugResourceIT {

    @Autowired
    private BugRepository bugRepository;

    @Autowired
    private BugMapper bugMapper;

    @Autowired
    private BugService bugService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restBugMockMvc;

    private Bug bug;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BugResource bugResource = new BugResource(bugService);
        this.restBugMockMvc = MockMvcBuilders.standaloneSetup(bugResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Bug createEntity(EntityManager em) {
        Bug bug = new Bug();
        return bug;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Bug createUpdatedEntity(EntityManager em) {
        Bug bug = new Bug();
        return bug;
    }

    @BeforeEach
    public void initTest() {
        bug = createEntity(em);
    }

    @Test
    @Transactional
    public void createBug() throws Exception {
        int databaseSizeBeforeCreate = bugRepository.findAll().size();

        // Create the Bug
        BugDTO bugDTO = bugMapper.toDto(bug);
        restBugMockMvc.perform(post("/api/bugs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bugDTO)))
            .andExpect(status().isCreated());

        // Validate the Bug in the database
        List<Bug> bugList = bugRepository.findAll();
        assertThat(bugList).hasSize(databaseSizeBeforeCreate + 1);
        Bug testBug = bugList.get(bugList.size() - 1);
    }

    @Test
    @Transactional
    public void createBugWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = bugRepository.findAll().size();

        // Create the Bug with an existing ID
        bug.setId(1L);
        BugDTO bugDTO = bugMapper.toDto(bug);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBugMockMvc.perform(post("/api/bugs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bugDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Bug in the database
        List<Bug> bugList = bugRepository.findAll();
        assertThat(bugList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllBugs() throws Exception {
        // Initialize the database
        bugRepository.saveAndFlush(bug);

        // Get all the bugList
        restBugMockMvc.perform(get("/api/bugs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bug.getId().intValue())));
    }
    
    @Test
    @Transactional
    public void getBug() throws Exception {
        // Initialize the database
        bugRepository.saveAndFlush(bug);

        // Get the bug
        restBugMockMvc.perform(get("/api/bugs/{id}", bug.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(bug.getId().intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingBug() throws Exception {
        // Get the bug
        restBugMockMvc.perform(get("/api/bugs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBug() throws Exception {
        // Initialize the database
        bugRepository.saveAndFlush(bug);

        int databaseSizeBeforeUpdate = bugRepository.findAll().size();

        // Update the bug
        Bug updatedBug = bugRepository.findById(bug.getId()).get();
        // Disconnect from session so that the updates on updatedBug are not directly saved in db
        em.detach(updatedBug);
        BugDTO bugDTO = bugMapper.toDto(updatedBug);

        restBugMockMvc.perform(put("/api/bugs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bugDTO)))
            .andExpect(status().isOk());

        // Validate the Bug in the database
        List<Bug> bugList = bugRepository.findAll();
        assertThat(bugList).hasSize(databaseSizeBeforeUpdate);
        Bug testBug = bugList.get(bugList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingBug() throws Exception {
        int databaseSizeBeforeUpdate = bugRepository.findAll().size();

        // Create the Bug
        BugDTO bugDTO = bugMapper.toDto(bug);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBugMockMvc.perform(put("/api/bugs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bugDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Bug in the database
        List<Bug> bugList = bugRepository.findAll();
        assertThat(bugList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBug() throws Exception {
        // Initialize the database
        bugRepository.saveAndFlush(bug);

        int databaseSizeBeforeDelete = bugRepository.findAll().size();

        // Delete the bug
        restBugMockMvc.perform(delete("/api/bugs/{id}", bug.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Bug> bugList = bugRepository.findAll();
        assertThat(bugList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
