package me.phrycenko.crowdtesting.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import me.phrycenko.crowdtesting.web.rest.TestUtil;

public class TesterDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TesterDTO.class);
        TesterDTO testerDTO1 = new TesterDTO();
        testerDTO1.setId(1L);
        TesterDTO testerDTO2 = new TesterDTO();
        assertThat(testerDTO1).isNotEqualTo(testerDTO2);
        testerDTO2.setId(testerDTO1.getId());
        assertThat(testerDTO1).isEqualTo(testerDTO2);
        testerDTO2.setId(2L);
        assertThat(testerDTO1).isNotEqualTo(testerDTO2);
        testerDTO1.setId(null);
        assertThat(testerDTO1).isNotEqualTo(testerDTO2);
    }
}
