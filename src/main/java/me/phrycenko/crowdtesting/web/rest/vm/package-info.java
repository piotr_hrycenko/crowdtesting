/**
 * View Models used by Spring MVC REST controllers.
 */
package me.phrycenko.crowdtesting.web.rest.vm;
