package me.phrycenko.crowdtesting.web.rest;

import me.phrycenko.crowdtesting.service.TesterService;
import me.phrycenko.crowdtesting.web.rest.errors.BadRequestAlertException;
import me.phrycenko.crowdtesting.service.dto.TesterDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link me.phrycenko.crowdtesting.domain.Tester}.
 */
@RestController
@RequestMapping("/api")
public class TesterResource {

    private final Logger log = LoggerFactory.getLogger(TesterResource.class);

    private static final String ENTITY_NAME = "tester";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TesterService testerService;

    public TesterResource(TesterService testerService) {
        this.testerService = testerService;
    }

    /**
     * {@code POST  /testers} : Create a new tester.
     *
     * @param testerDTO the testerDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new testerDTO, or with status {@code 400 (Bad Request)} if the tester has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/testers")
    public ResponseEntity<TesterDTO> createTester(@Valid @RequestBody TesterDTO testerDTO) throws URISyntaxException {
        log.debug("REST request to save Tester : {}", testerDTO);
        if (testerDTO.getId() != null) {
            throw new BadRequestAlertException("A new tester cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TesterDTO result = testerService.save(testerDTO);
        return ResponseEntity.created(new URI("/api/testers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /testers} : Updates an existing tester.
     *
     * @param testerDTO the testerDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated testerDTO,
     * or with status {@code 400 (Bad Request)} if the testerDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the testerDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/testers")
    public ResponseEntity<TesterDTO> updateTester(@Valid @RequestBody TesterDTO testerDTO) throws URISyntaxException {
        log.debug("REST request to update Tester : {}", testerDTO);
        if (testerDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TesterDTO result = testerService.save(testerDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, testerDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /testers} : get all the testers.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of testers in body.
     */
    @GetMapping("/testers")
    public List<TesterDTO> getAllTesters() {
        log.debug("REST request to get all Testers");
        return testerService.findAll();
    }

    /**
     * {@code GET  /testers/:id} : get the "id" tester.
     *
     * @param id the id of the testerDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the testerDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/testers/{id}")
    public ResponseEntity<TesterDTO> getTester(@PathVariable Long id) {
        log.debug("REST request to get Tester : {}", id);
        Optional<TesterDTO> testerDTO = testerService.findOne(id);
        return ResponseUtil.wrapOrNotFound(testerDTO);
    }

    /**
     * {@code DELETE  /testers/:id} : delete the "id" tester.
     *
     * @param id the id of the testerDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/testers/{id}")
    public ResponseEntity<Void> deleteTester(@PathVariable Long id) {
        log.debug("REST request to delete Tester : {}", id);
        testerService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
