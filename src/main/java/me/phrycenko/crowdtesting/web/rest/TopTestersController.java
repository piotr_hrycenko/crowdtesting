package me.phrycenko.crowdtesting.web.rest;

import me.phrycenko.crowdtesting.domain.enumeration.Country;
import me.phrycenko.crowdtesting.domain.projection.TesterStatProjection;
import me.phrycenko.crowdtesting.service.TopTestersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * REST controller for top testers statistics.
 */
@RestController
@RequestMapping("/api")
public class TopTestersController {

    private final Logger log = LoggerFactory.getLogger(TopTestersController.class);

    private final TopTestersService testerDeviceStatService;

    public TopTestersController(TopTestersService testerDeviceStatService) {
        this.testerDeviceStatService = testerDeviceStatService;
    }

    /**
     * {@code GET  /top-testers} : get top testers.
     *

     * @param countries - countries to be filtered.
     * @param devicesIds - devicesIds to be filtered.
     * @param pageSize - page size for pagination.
     * @param offset - offset for pagination.

     * @return the {@link List} with list of TesterStatProjection in body.
     */
    @GetMapping("/top-testers")
    public List<TesterStatProjection> getTopTesters(
        @RequestParam(defaultValue = "", name = "countries") List<Country> countries,
        @RequestParam(defaultValue = "", name = "devices") List<Integer> devicesIds,
        @RequestParam(defaultValue = "20", name = "pageSize") Integer pageSize,
        @RequestParam(defaultValue = "0", name = "offset") Integer offset
    ) {

        String commaSeparatedCountries = "ALL";

        if( (null != countries) && (!countries.isEmpty()) ) {
            commaSeparatedCountries = countries.stream()
                .map(country -> country.toString())
                .collect(Collectors.joining(", "));
        }

        String commaSeparatedDevicesIds = "ALL";

        if( (null != devicesIds) && (!devicesIds.isEmpty()) ) {
            commaSeparatedDevicesIds = devicesIds.stream()
                .map(id -> id.toString())
                .collect(Collectors.joining(", "));
        }

        log.debug(String.format("REST request to get a page of top testers [ pageSize: %s ] - [ offset: %s ] - " +
            "[ countries: %s ] - [ devicesIds: %s ]", pageSize, offset, commaSeparatedCountries, commaSeparatedDevicesIds));

//        log.debug("REST request to get a page of top testers " +
//                "[ pageSize: " + pageSize + "] - [ offset: " + offset + " ]" +
//                "[ countries: " + commaSeparatedCountries + "] - [ devicesIds: " + commaSeparatedDevicesIds + " ]");

        return testerDeviceStatService.findAllByCountriesAndDevices(countries, devicesIds, pageSize, offset);
    }

}
