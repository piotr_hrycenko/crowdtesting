package me.phrycenko.crowdtesting.repository;

import me.phrycenko.crowdtesting.domain.projection.TesterStatProjection;
//import org.springframework.data.jpa.repository.*;
//import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import java.util.List;


/**
 * Spring Data  repository for the TesterStatProjection.
 */
@Repository
public class TesterDeviceStatRepositoryImpl implements TesterDeviceStatRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    @SuppressWarnings("unchecked")
    public List<TesterStatProjection> findAllTopExperienced(int pageSize, int offset) {

        Query query = this.em.createNamedQuery("TesterStatProjection.findAllTopExperienced");
        query.setParameter("pageSize", pageSize);
        query.setParameter("offset", offset);
        return query.getResultList();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<TesterStatProjection> findAllByCountryTopExperienced(List<String> countries, int pageSize, int offset) {

        Query query = this.em.createNamedQuery("TesterStatProjection.findAllByCountryTopExperienced");
        query.setParameter("pageSize", pageSize);
        query.setParameter("offset", offset);
        query.setParameter("countries", countries);
        return query.getResultList();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<TesterStatProjection> findAllByDeviceIdsTopExperienced(List<Integer> deviceIds, int pageSize,
                                                                       int offset) {

        Query query = this.em.createNamedQuery("TesterStatProjection.findAllByDeviceIdsTopExperienced");
        query.setParameter("pageSize", pageSize);
        query.setParameter("offset", offset);
        query.setParameter("deviceIds", deviceIds);
        return query.getResultList();
    }


    @Override
    @SuppressWarnings("unchecked")
    public List<TesterStatProjection> findAllByCountryAndDeviceIdsTopExperienced(List<String> countries,
                                                                          List<Integer> deviceIds,
                                                                          int pageSize, int offset) {

        Query query = this.em.createNamedQuery("TesterStatProjection.findAllByCountryAndDeviceIdsTopExperienced");
        query.setParameter("pageSize", pageSize);
        query.setParameter("offset", offset);
        query.setParameter("countries", countries);
        query.setParameter("deviceIds", deviceIds);
        return query.getResultList();
    }

}
