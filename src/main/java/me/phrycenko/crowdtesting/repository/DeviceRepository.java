package me.phrycenko.crowdtesting.repository;

import me.phrycenko.crowdtesting.domain.Device;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Device entity.
 */
@Repository
public interface DeviceRepository extends JpaRepository<Device, Long> {

    @Query(value = "select distinct device from Device device left join fetch device.testers",
        countQuery = "select count(distinct device) from Device device")
    Page<Device> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct device from Device device left join fetch device.testers")
    List<Device> findAllWithEagerRelationships();

    @Query("select device from Device device left join fetch device.testers where device.id =:id")
    Optional<Device> findOneWithEagerRelationships(@Param("id") Long id);

}
