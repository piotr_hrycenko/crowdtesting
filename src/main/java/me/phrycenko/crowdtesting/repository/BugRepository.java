package me.phrycenko.crowdtesting.repository;

import me.phrycenko.crowdtesting.domain.Bug;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Bug entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BugRepository extends JpaRepository<Bug, Long> {

}
