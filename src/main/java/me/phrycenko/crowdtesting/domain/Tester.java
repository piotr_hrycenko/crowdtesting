package me.phrycenko.crowdtesting.domain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import me.phrycenko.crowdtesting.domain.projection.TesterStatProjection;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import me.phrycenko.crowdtesting.domain.enumeration.Country;

/**
 * The Tester entity.
 */
@Entity
@Table(name = "tester")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)

@NamedNativeQueries({
    @NamedNativeQuery(
        name = "TesterStatProjection.findAllTopExperienced",
        query = "SELECT"
            + "  t.id AS testerId, "
            + "  t.first_name AS firstName, "
            + "  t.last_name AS lastName, "
            + "  s.sum_bugs_amount AS bugsAmount "
            + "FROM tester t INNER JOIN"
            + "  ("
            + "    SELECT SUM(tds.bugs_amount) AS sum_bugs_amount, tds.tester_id "
            + "     FROM tester_device_stat tds "
            + "     GROUP BY tds.tester_id "
            + "     ORDER BY sum_bugs_amount DESC "
            + "     LIMIT :pageSize OFFSET :offset "
            + "  ) AS s "
            + "  ON t.id = s.tester_id "
            + "  ORDER BY bugsAmount DESC ",
        resultSetMapping = "TesterStatProjection"
    ),
    @NamedNativeQuery(
        name = "TesterStatProjection.findAllByCountryTopExperienced",
        query = "SELECT"
            + "  t.id AS testerId, "
            + "  t.first_name AS firstName, "
            + "  t.last_name AS lastName, "
            + "  s.sum_bugs_amount AS bugsAmount "
            + "FROM tester t INNER JOIN"
            + "  ("
            + "    SELECT SUM(tds.bugs_amount) AS sum_bugs_amount, tds.tester_id "
            + "     FROM tester_device_stat tds "
            + "     WHERE tds.country IN (:countries) "
            + "     GROUP BY tds.tester_id "
            + "     ORDER BY sum_bugs_amount DESC "
            + "     LIMIT :pageSize OFFSET :offset "
            + "  ) AS s "
            + "  ON t.id = s.tester_id "
            + "  ORDER BY bugsAmount DESC ",
        resultSetMapping = "TesterStatProjection"
    ),
    @NamedNativeQuery(
        name = "TesterStatProjection.findAllByDeviceIdsTopExperienced",
        query = "SELECT"
            + "  t.id AS testerId, "
            + "  t.first_name AS firstName, "
            + "  t.last_name AS lastName, "
            + "  s.sum_bugs_amount AS bugsAmount "
            + "FROM tester t INNER JOIN"
            + "  ("
            + "    SELECT SUM(tds.bugs_amount) AS sum_bugs_amount, tds.tester_id "
            + "     FROM tester_device_stat tds "
            + "     WHERE tds.device_id IN (:deviceIds) "
            + "     GROUP BY tds.tester_id "
            + "     ORDER BY sum_bugs_amount DESC "
            + "     LIMIT :pageSize OFFSET :offset "
            + "  ) AS s "
            + "  ON t.id = s.tester_id "
            + "  ORDER BY bugsAmount DESC ",
        resultSetMapping = "TesterStatProjection"
    ),
    @NamedNativeQuery(
        name = "TesterStatProjection.findAllByCountryAndDeviceIdsTopExperienced",
        query = "SELECT"
            + "  t.id AS testerId, "
            + "  t.first_name AS firstName, "
            + "  t.last_name AS lastName, "
            + "  s.sum_bugs_amount AS bugsAmount "
            + "FROM tester t INNER JOIN"
            + "  ("
            + "    SELECT SUM(tds.bugs_amount) AS sum_bugs_amount, tds.tester_id "
            + "     FROM tester_device_stat tds "
            + "     WHERE tds.device_id IN (:deviceIds) "
            + "     AND tds.country IN (:countries) "
            + "     GROUP BY tds.tester_id "
            + "     ORDER BY sum_bugs_amount DESC "
            + "     LIMIT :pageSize OFFSET :offset "
            + "  ) AS s "
            + "  ON t.id = s.tester_id "
            + "  ORDER BY bugsAmount DESC ",
        resultSetMapping = "TesterStatProjection"
    )
})
@SqlResultSetMapping(
    name = "TesterStatProjection",
    classes = @ConstructorResult(
        targetClass = TesterStatProjection.class,
        columns = {
            @ColumnResult(name="testerId", type=Long.class),
            @ColumnResult(name="firstName", type=String.class),
            @ColumnResult(name="lastName", type=String.class),
            @ColumnResult(name="bugsAmount", type=Integer.class)
        }
    )
)

public class Tester implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "first_name", nullable = false)
    private String firstName;

    @NotNull
    @Column(name = "last_name", nullable = false)
    private String lastName;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "country", nullable = false)
    private Country country;

    @Column(name = "last_login")
    private LocalDate lastLogin;

    @OneToMany(mappedBy = "tester")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Bug> testers = new HashSet<>();

    @ManyToMany(mappedBy = "testers")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnore
    private Set<Device> devices = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public Tester firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Tester lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Country getCountry() {
        return country;
    }

    public Tester country(Country country) {
        this.country = country;
        return this;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public LocalDate getLastLogin() {
        return lastLogin;
    }

    public Tester lastLogin(LocalDate lastLogin) {
        this.lastLogin = lastLogin;
        return this;
    }

    public void setLastLogin(LocalDate lastLogin) {
        this.lastLogin = lastLogin;
    }

    public Set<Bug> getTesters() {
        return testers;
    }

    public Tester testers(Set<Bug> bugs) {
        this.testers = bugs;
        return this;
    }

    public Tester addTester(Bug bug) {
        this.testers.add(bug);
        bug.setTester(this);
        return this;
    }

    public Tester removeTester(Bug bug) {
        this.testers.remove(bug);
        bug.setTester(null);
        return this;
    }

    public void setTesters(Set<Bug> bugs) {
        this.testers = bugs;
    }

    public Set<Device> getDevices() {
        return devices;
    }

    public Tester devices(Set<Device> devices) {
        this.devices = devices;
        return this;
    }

    public Tester addDevice(Device device) {
        this.devices.add(device);
        device.getTesters().add(this);
        return this;
    }

    public Tester removeDevice(Device device) {
        this.devices.remove(device);
        device.getTesters().remove(this);
        return this;
    }

    public void setDevices(Set<Device> devices) {
        this.devices = devices;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Tester)) {
            return false;
        }
        return id != null && id.equals(((Tester) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Tester{" +
            "id=" + getId() +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", country='" + getCountry() + "'" +
            ", lastLogin='" + getLastLogin() + "'" +
            "}";
    }
}
