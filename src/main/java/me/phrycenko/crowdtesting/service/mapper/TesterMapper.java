package me.phrycenko.crowdtesting.service.mapper;

import me.phrycenko.crowdtesting.domain.*;
import me.phrycenko.crowdtesting.service.dto.TesterDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Tester} and its DTO {@link TesterDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TesterMapper extends EntityMapper<TesterDTO, Tester> {


    @Mapping(target = "testers", ignore = true)
    @Mapping(target = "removeTester", ignore = true)
    @Mapping(target = "devices", ignore = true)
    @Mapping(target = "removeDevice", ignore = true)
    Tester toEntity(TesterDTO testerDTO);

    default Tester fromId(Long id) {
        if (id == null) {
            return null;
        }
        Tester tester = new Tester();
        tester.setId(id);
        return tester;
    }
}
