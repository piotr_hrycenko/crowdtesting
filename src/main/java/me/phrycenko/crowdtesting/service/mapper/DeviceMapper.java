package me.phrycenko.crowdtesting.service.mapper;

import me.phrycenko.crowdtesting.domain.*;
import me.phrycenko.crowdtesting.service.dto.DeviceDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Device} and its DTO {@link DeviceDTO}.
 */
@Mapper(componentModel = "spring", uses = {TesterMapper.class})
public interface DeviceMapper extends EntityMapper<DeviceDTO, Device> {


    @Mapping(target = "removeTester", ignore = true)

    default Device fromId(Long id) {
        if (id == null) {
            return null;
        }
        Device device = new Device();
        device.setId(id);
        return device;
    }
}
