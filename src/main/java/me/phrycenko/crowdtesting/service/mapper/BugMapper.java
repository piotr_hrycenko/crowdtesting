package me.phrycenko.crowdtesting.service.mapper;

import me.phrycenko.crowdtesting.domain.*;
import me.phrycenko.crowdtesting.service.dto.BugDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Bug} and its DTO {@link BugDTO}.
 */
@Mapper(componentModel = "spring", uses = {DeviceMapper.class, TesterMapper.class})
public interface BugMapper extends EntityMapper<BugDTO, Bug> {

    @Mapping(source = "device.id", target = "deviceId")
    @Mapping(source = "tester.id", target = "testerId")
    BugDTO toDto(Bug bug);

    @Mapping(source = "deviceId", target = "device")
    @Mapping(source = "testerId", target = "tester")
    Bug toEntity(BugDTO bugDTO);

    default Bug fromId(Long id) {
        if (id == null) {
            return null;
        }
        Bug bug = new Bug();
        bug.setId(id);
        return bug;
    }
}
