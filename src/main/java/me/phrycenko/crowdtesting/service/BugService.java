package me.phrycenko.crowdtesting.service;

import me.phrycenko.crowdtesting.domain.Bug;
import me.phrycenko.crowdtesting.repository.BugRepository;
import me.phrycenko.crowdtesting.service.dto.BugDTO;
import me.phrycenko.crowdtesting.service.mapper.BugMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Bug}.
 */
@Service
@Transactional
public class BugService {

    private final Logger log = LoggerFactory.getLogger(BugService.class);

    private final BugRepository bugRepository;

    private final BugMapper bugMapper;

    public BugService(BugRepository bugRepository, BugMapper bugMapper) {
        this.bugRepository = bugRepository;
        this.bugMapper = bugMapper;
    }

    /**
     * Save a bug.
     *
     * @param bugDTO the entity to save.
     * @return the persisted entity.
     */
    public BugDTO save(BugDTO bugDTO) {
        log.debug("Request to save Bug : {}", bugDTO);
        Bug bug = bugMapper.toEntity(bugDTO);
        bug = bugRepository.save(bug);
        return bugMapper.toDto(bug);
    }

    /**
     * Get all the bugs.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<BugDTO> findAll() {
        log.debug("Request to get all Bugs");
        return bugRepository.findAll().stream()
            .map(bugMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one bug by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<BugDTO> findOne(Long id) {
        log.debug("Request to get Bug : {}", id);
        return bugRepository.findById(id)
            .map(bugMapper::toDto);
    }

    /**
     * Delete the bug by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Bug : {}", id);
        bugRepository.deleteById(id);
    }
}
