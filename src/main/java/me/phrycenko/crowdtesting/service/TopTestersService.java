package me.phrycenko.crowdtesting.service;

import me.phrycenko.crowdtesting.domain.enumeration.Country;
import me.phrycenko.crowdtesting.domain.projection.TesterStatProjection;
import me.phrycenko.crowdtesting.repository.TesterDeviceStatRepositoryImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link TesterStatProjection}.
 */
@Service
@Transactional
public class TopTestersService {

    private final Logger log = LoggerFactory.getLogger(TopTestersService.class);

    private final TesterDeviceStatRepositoryImpl testerDeviceStatRepository;

    public TopTestersService(TesterDeviceStatRepositoryImpl testerDeviceStatRepository) {
        this.testerDeviceStatRepository = testerDeviceStatRepository;
    }

    /**
     * Get all the testerDeviceStats.
     *
     * @param countries the countries used for filtering.
     * @param devicesIds the devices used for filtering.
     * @param pageSize the pagination page size information.
     * @param offset the pagination offset information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<TesterStatProjection> findAllByCountriesAndDevices(List<Country> countries, List<Integer> devicesIds,
                                                                   Integer pageSize, Integer offset) {
        log.debug("Request to get all TesterDeviceStats by countries and devices");
        List<String> countriesS = countries.stream()
            .map(country -> country.name())
            .collect(Collectors.toList());

        if(countriesS.isEmpty() && devicesIds.isEmpty()) {
            return testerDeviceStatRepository.findAllTopExperienced(pageSize, offset);
        }

        if(countriesS.isEmpty()) {
            return testerDeviceStatRepository.findAllByDeviceIdsTopExperienced(devicesIds, pageSize, offset);
        }

        if(devicesIds.isEmpty()) {
            return testerDeviceStatRepository.findAllByCountryTopExperienced(countriesS, pageSize, offset);
        }

        return testerDeviceStatRepository.findAllByCountryAndDeviceIdsTopExperienced(countriesS, devicesIds, pageSize, offset);
    }

}
