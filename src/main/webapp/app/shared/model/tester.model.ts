import { Moment } from 'moment';
import { IBug } from 'app/shared/model/bug.model';
import { IDevice } from 'app/shared/model/device.model';
import { Country } from 'app/shared/model/enumerations/country.model';

export interface ITester {
  id?: number;
  firstName?: string;
  lastName?: string;
  country?: Country;
  lastLogin?: Moment;
  testers?: IBug[];
  devices?: IDevice[];
}

export class Tester implements ITester {
  constructor(
    public id?: number,
    public firstName?: string,
    public lastName?: string,
    public country?: Country,
    public lastLogin?: Moment,
    public testers?: IBug[],
    public devices?: IDevice[]
  ) {}
}
