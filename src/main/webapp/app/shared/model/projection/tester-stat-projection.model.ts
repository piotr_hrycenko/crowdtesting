export interface ITesterStatProjection {
  testerId?: number;
  firstName?: String;
  lastName?: String;
  bugsAmount?: number;
}

export class TesterStatProjection implements ITesterStatProjection {
  constructor(
    public testerId?: number,
    public firstName?: String,
    public lastName?: String,
    public bugsAmount?: number
  ) {}
}
