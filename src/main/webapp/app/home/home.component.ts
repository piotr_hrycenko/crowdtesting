import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { LoginModalService } from 'app/core/login/login-modal.service';
import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/user/account.model';
import { ITesterStatProjection } from 'app/shared/model/projection/tester-stat-projection.model';
import { HomeService } from "app/home/home.service";
import {HttpHeaders, HttpResponse} from "@angular/common/http";

@Component({
  selector: 'jhi-home',
  templateUrl: './home.component.html',
  styleUrls: ['home.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  account: Account | null = null;
  authSubscription?: Subscription;
  testerStatProjections: ITesterStatProjection[];

  constructor(private accountService: AccountService,
              private loginModalService: LoginModalService,
              protected homeService: HomeService
  ) {
    this.testerStatProjections = [];
  }

  ngOnInit(): void {
    this.authSubscription = this.accountService.getAuthenticationState().subscribe(account => (this.account = account));
    this.homeService
      .query({})
      .subscribe((res: HttpResponse<ITesterStatProjection[]>) => this.paginateTesterStatProjections(res.body, res.headers));
  }

  isAuthenticated(): boolean {
    return this.accountService.isAuthenticated();
  }

  login(): void {
    this.loginModalService.open();
  }

  ngOnDestroy(): void {
    if (this.authSubscription) {
      this.authSubscription.unsubscribe();
    }
  }

  protected paginateTesterStatProjections(data: ITesterStatProjection[] | null, headers: HttpHeaders): void {
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.testerStatProjections.push(data[i]);
      }
    }
  }

}
