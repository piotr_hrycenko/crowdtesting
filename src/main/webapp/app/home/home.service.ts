import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ITesterStatProjection } from 'app/shared/model/projection/tester-stat-projection.model';

type EntityResponseType = HttpResponse<ITesterStatProjection>;
type EntityArrayResponseType = HttpResponse<ITesterStatProjection[]>;

@Injectable({ providedIn: 'root' })
export class HomeService {
  public resourceUrl = SERVER_API_URL + 'api/top-testers';

  constructor(protected http: HttpClient) {}

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ITesterStatProjection[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

}
