import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgOptionHighlightModule } from '@ng-select/ng-option-highlight';

import { CrowdtestingSharedModule } from 'app/shared/shared.module';
import { HOME_ROUTE } from './home.route';
import { HomeComponent } from './home.component';
import { TopTestersComponent } from './toptesters/top-testers.component';

@NgModule({
  imports: [
    CrowdtestingSharedModule,
    NgSelectModule,
    NgOptionHighlightModule,
    RouterModule.forChild([HOME_ROUTE])
  ],
  declarations: [HomeComponent, TopTestersComponent]
})
export class CrowdtestingHomeModule {}
