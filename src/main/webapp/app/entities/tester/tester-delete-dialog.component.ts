import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITester } from 'app/shared/model/tester.model';
import { TesterService } from './tester.service';

@Component({
  templateUrl: './tester-delete-dialog.component.html'
})
export class TesterDeleteDialogComponent {
  tester?: ITester;

  constructor(protected testerService: TesterService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.testerService.delete(id).subscribe(() => {
      this.eventManager.broadcast('testerListModification');
      this.activeModal.close();
    });
  }
}
