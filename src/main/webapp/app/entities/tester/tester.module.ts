import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CrowdtestingSharedModule } from 'app/shared/shared.module';
import { TesterComponent } from './tester.component';
import { TesterDetailComponent } from './tester-detail.component';
import { TesterUpdateComponent } from './tester-update.component';
import { TesterDeleteDialogComponent } from './tester-delete-dialog.component';
import { testerRoute } from './tester.route';

@NgModule({
  imports: [CrowdtestingSharedModule, RouterModule.forChild(testerRoute)],
  declarations: [TesterComponent, TesterDetailComponent, TesterUpdateComponent, TesterDeleteDialogComponent],
  entryComponents: [TesterDeleteDialogComponent]
})
export class CrowdtestingTesterModule {}
