import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ITester } from 'app/shared/model/tester.model';

type EntityResponseType = HttpResponse<ITester>;
type EntityArrayResponseType = HttpResponse<ITester[]>;

@Injectable({ providedIn: 'root' })
export class TesterService {
  public resourceUrl = SERVER_API_URL + 'api/testers';

  constructor(protected http: HttpClient) {}

  create(tester: ITester): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(tester);
    return this.http
      .post<ITester>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(tester: ITester): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(tester);
    return this.http
      .put<ITester>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ITester>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ITester[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(tester: ITester): ITester {
    const copy: ITester = Object.assign({}, tester, {
      lastLogin: tester.lastLogin && tester.lastLogin.isValid() ? tester.lastLogin.format(DATE_FORMAT) : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.lastLogin = res.body.lastLogin ? moment(res.body.lastLogin) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((tester: ITester) => {
        tester.lastLogin = tester.lastLogin ? moment(tester.lastLogin) : undefined;
      });
    }
    return res;
  }
}
