import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ITester } from 'app/shared/model/tester.model';
import { TesterService } from './tester.service';
import { TesterDeleteDialogComponent } from './tester-delete-dialog.component';

@Component({
  selector: 'jhi-tester',
  templateUrl: './tester.component.html'
})
export class TesterComponent implements OnInit, OnDestroy {
  testers?: ITester[];
  eventSubscriber?: Subscription;

  constructor(protected testerService: TesterService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.testerService.query().subscribe((res: HttpResponse<ITester[]>) => {
      this.testers = res.body ? res.body : [];
    });
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInTesters();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ITester): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInTesters(): void {
    this.eventSubscriber = this.eventManager.subscribe('testerListModification', () => this.loadAll());
  }

  delete(tester: ITester): void {
    const modalRef = this.modalService.open(TesterDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.tester = tester;
  }
}
