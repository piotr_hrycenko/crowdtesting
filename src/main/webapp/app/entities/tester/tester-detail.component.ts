import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITester } from 'app/shared/model/tester.model';

@Component({
  selector: 'jhi-tester-detail',
  templateUrl: './tester-detail.component.html'
})
export class TesterDetailComponent implements OnInit {
  tester: ITester | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ tester }) => {
      this.tester = tester;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
