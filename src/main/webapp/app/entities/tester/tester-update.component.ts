import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';

import { ITester, Tester } from 'app/shared/model/tester.model';
import { TesterService } from './tester.service';

@Component({
  selector: 'jhi-tester-update',
  templateUrl: './tester-update.component.html'
})
export class TesterUpdateComponent implements OnInit {
  isSaving = false;
  lastLoginDp: any;

  editForm = this.fb.group({
    id: [],
    firstName: [null, [Validators.required]],
    lastName: [null, [Validators.required]],
    country: [null, [Validators.required]],
    lastLogin: []
  });

  constructor(protected testerService: TesterService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ tester }) => {
      this.updateForm(tester);
    });
  }

  updateForm(tester: ITester): void {
    this.editForm.patchValue({
      id: tester.id,
      firstName: tester.firstName,
      lastName: tester.lastName,
      country: tester.country,
      lastLogin: tester.lastLogin
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const tester = this.createFromForm();
    if (tester.id !== undefined) {
      this.subscribeToSaveResponse(this.testerService.update(tester));
    } else {
      this.subscribeToSaveResponse(this.testerService.create(tester));
    }
  }

  private createFromForm(): ITester {
    return {
      ...new Tester(),
      id: this.editForm.get(['id'])!.value,
      firstName: this.editForm.get(['firstName'])!.value,
      lastName: this.editForm.get(['lastName'])!.value,
      country: this.editForm.get(['country'])!.value,
      lastLogin: this.editForm.get(['lastLogin'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITester>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
