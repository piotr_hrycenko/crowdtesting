
/**
  Update tester device stat country
 */

CREATE FUNCTION public.update_tester_device_stat_country()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    NOT LEAKPROOF
AS $BODY$
BEGIN
    IF NEW.country != OLD.country
    THEN
        UPDATE public.tester_device_stat
        SET country = NEW.country
        WHERE tester_id = OLD.id;
    END IF;
    RETURN NEW;
END;
$BODY$;

ALTER FUNCTION public.update_tester_device_stat_country()
    OWNER TO postgres;

CREATE TRIGGER update_tester_device_stat_country_on_tester_update
    AFTER UPDATE
    ON public.tester
    FOR EACH ROW
EXECUTE PROCEDURE public.update_tester_device_stat_country();

