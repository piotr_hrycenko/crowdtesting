
/**
  Increment tester device stat bugs amount
 */

CREATE FUNCTION public.increment_tester_device_stat_bugs_amount()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    NOT LEAKPROOF
AS $BODY$
BEGIN
    INSERT INTO public.tester_device_stat (country, bugs_amount, tester_id, device_id)
    SELECT tester.country, 1, NEW.tester_id, NEW.device_id
    FROM public.tester
    WHERE tester.id = NEW.tester_id
    ON CONFLICT (tester_id, device_id)
        DO
            UPDATE
        SET bugs_amount = public.tester_device_stat.bugs_amount + 1;
    RETURN NEW;
END;
$BODY$;

ALTER FUNCTION public.increment_tester_device_stat_bugs_amount()
    OWNER TO postgres;

CREATE TRIGGER increment_tester_device_stat_bugs_amount_on_new_bug
    AFTER INSERT
    ON public.bug
    FOR EACH ROW
EXECUTE PROCEDURE public.increment_tester_device_stat_bugs_amount();


/**
  Decrement tester device stat bugs amount
 */

CREATE FUNCTION public.decrement_tester_device_stat_bugs_amount()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    NOT LEAKPROOF
AS $BODY$
BEGIN
    UPDATE public.tester_device_stat
    SET bugs_amount = public.tester_device_stat.bugs_amount - 1
    WHERE tester_id = OLD.tester_id AND device_id = OLD.device_id;
    RETURN OLD;
END;
$BODY$;

ALTER FUNCTION public.decrement_tester_device_stat_bugs_amount()
    OWNER TO postgres;

CREATE TRIGGER decrement_tester_device_stat_bugs_amount_on_bug_delete
    AFTER DELETE
    ON public.bug
    FOR EACH ROW
EXECUTE PROCEDURE public.decrement_tester_device_stat_bugs_amount();

